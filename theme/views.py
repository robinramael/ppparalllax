from django.views.generic import TemplateView

from theme.models import FeaturedBlogPost


class HomeView(TemplateView):
    template_name = 'home.html'

    def get_context_data(self, **kwargs):
        return {'featured_blogposts': FeaturedBlogPost.objects.currently_featured()}

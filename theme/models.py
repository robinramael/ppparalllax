from adminsortable.models import SortableMixin
from django.db import models
from django.db.models import Q
from django.db.models.signals import post_save
from django.dispatch import receiver
from django.utils import timezone
from django.utils.translation import ugettext_lazy as _
from djchoices import ChoiceItem
from djchoices import DjangoChoices
from mezzanine.blog.models import BlogPost
from mezzanine.core.fields import FileField
from mezzanine.utils.models import upload_to


class FeaturedBlogPostManager(models.Manager):
    def currently_featured(self):
        now = timezone.now()

        return self.exclude(enabled=False).filter((Q(active_from__lte=now) | Q(active_from__isnull=True)) &
                                                  Q(active_to__gte=now) | Q(active_to__isnull=True)).order_by('order')


class FeaturedBlogPost(SortableMixin):
    post = models.ForeignKey(BlogPost)

    enabled = models.BooleanField(default=True)

    active_from = models.DateTimeField(blank=True, null=True)
    active_to = models.DateTimeField(blank=True, null=True)

    title_override = models.CharField(max_length=255, null=True, blank=True)
    image_overide = FileField(verbose_name=_("Featured Image"),
                              upload_to=upload_to("theme.FeaturedBlogPost.image_override", "theme"),
                              format="Image", max_length=255, null=True, blank=True)

    subtitle = models.TextField(blank=True, null=True)

    class PlacementChoices(DjangoChoices):
        right = ChoiceItem("right")
        left = ChoiceItem("left")
        full_width = ChoiceItem("full")

    placement = models.CharField(max_length=10, choices=PlacementChoices.choices,
                                 validators=[PlacementChoices.validator])

    order = models.PositiveIntegerField(default=0, editable=False, db_index=True)

    @property
    def display_image(self):
        return self.image_overide or self.post.featured_image

    objects = FeaturedBlogPostManager()

    class Meta:
        ordering = ['order']

    def __str__(self):
        return "{}".format(self.post.title)


class Author(models.Model):
    name = models.CharField(max_length=200)
    bio = models.TextField(blank=True, null=True)
    birthyear = models.PositiveIntegerField(blank=True, null=True)

    image = FileField(verbose_name=_("Featured Image"),
                      upload_to=upload_to("theme.Author.image", "theme"),
                      format="Image", max_length=255, null=True, blank=True)

    def __str__(self):
        return "{name} ({year})".format(name=self.name, year=self.birthyear)


class BlogPostProperties(models.Model):
    blogpost = models.OneToOneField(BlogPost, related_name='properties')
    authors = models.ManyToManyField(Author, blank=True)

    @property
    def authored_title(self):
        if self.authors.first():
            author_str = ' & '.join(author.name for author in self.authors.all())
            return '{} ({})'.format(self.blogpost.title, author_str)
        else:
            return self.blogpost.title

    @property
    def display_image(self):
        feature = self.blogpost.featuredblogpost_set.first()
        return feature.display_image if feature else self.blogpost.featured_image


@receiver(post_save, sender=BlogPost)
def add_properties(sender, **kwargs):
    post = kwargs.pop('instance')

    BlogPostProperties.objects.get_or_create(blogpost=post)

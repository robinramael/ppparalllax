from adminsortable.admin import SortableAdmin
from django.contrib import admin
from django.contrib.admin.options import TabularInline
from mezzanine.blog.admin import BlogPostAdmin
from mezzanine.blog.models import BlogPost

from theme.models import FeaturedBlogPost, BlogPostProperties, Author


class BlogPostPropertiesInline(TabularInline):
    model = BlogPostProperties


class ExtendedBlogPostAdmin(BlogPostAdmin):
    inlines = BlogPostAdmin.inlines + [BlogPostPropertiesInline]

    list_display = BlogPostAdmin.list_display + ['authors']

    def authors(self, blogpost):
        return ', '.join(author.name for author in blogpost.properties.authors.all())


class FeaturedBlogPostAdmin(SortableAdmin):
    list_display = ('post', 'placement', 'enabled', 'active_from', 'active_to')


admin.site.register(Author)
admin.site.unregister(BlogPost)
admin.site.register(BlogPost, ExtendedBlogPostAdmin)
admin.site.register(FeaturedBlogPost, FeaturedBlogPostAdmin)

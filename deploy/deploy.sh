cd ~/ppparalllax/ && git checkout master && git pull origin master

. ~/.virtualenvs/ppparalllax/bin/activate && pip install -r requirements.txt
python manage.py migrate
compass compile
python manage.py collectstatic --no-input
touch /var/www/www_ppparalllax_org_wsgi.py
